unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Menus, Inifiles, fphttpclient, LazUtf8, httpprotocol, ssockets,
  {$IFDEF Windows}MMSystem, ShellApi,{$ELSE}process,{$ENDIF}
  lazlogger;

const
  VERSION = '0.1.1';
  INIFILE = 'omtray.ini';
  DEF_SECTION = 'Default';
  INIICONSPATH = 'IconsPath';
  INISOUNDSPATH = 'SoundsPath';
  DEF_RESOURCEPATH = 'resource';
  INIDISPLAYURL = 'DisplayURL';
  INIHTTPUSERANME = 'HTTPUserName';
  INIHTTPPASSWORD = 'HTTPPassword';
  INIPROXYHOST = 'ProxyHost';
  INIPROXYPORT = 'ProxyPort';
  DEF_PROXYPORT = 3128;
  INIPROXYUSERANME = 'ProxyUserName';
  INIPROXYPASSWORD = 'ProxyPassword';
  INIPOLLTIME = 'PollTime';
  DEF_POLLTIME = 60;
  INIPAGEDELAY = 'PageDelay';
  DEF_PAGEDELAY = 3600;
  INIPLAYCOMMAND = 'NonWindowsPlayCommand';
  POPUPLEVELS = 'r,g,p,y';
  INIPOPUPLEVELS = 'PopupLevels';


type

  { TMainForm }

  TMainForm = class(TForm)
    MainTimer: TTimer;
    CycleTimer: TTimer;
    Exit1: TMenuItem;
    EditIniFile: TMenuItem;
    CheckInstantly: TMenuItem;
    ShowPopup: TMenuItem;
    ReloadConfig: TMenuItem;
    N1: TMenuItem;
    OpenInBrowser: TMenuItem;
    About: TMenuItem;
    PopupMenu1: TPopupMenu;
    TrayIcon: TTrayIcon;
    procedure AboutOnClick(Sender: TObject);
    procedure CheckInstantlyOnClick(Sender: TObject);
    procedure CycleTimerTimer(Sender: TObject);
    procedure EditIniFileOnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MainTimerTimer(Sender: TObject);
    procedure ShowPopupClick(Sender: TObject);
    procedure OnExitClick(Sender: TObject);
    procedure OnReloadIniOnClick(Sender: TObject);
    procedure OpenInBrowserClick(Sender: TObject);
    procedure TrayIconDoubleClick(Sender: TObject);

  private
    FirstAlertTime: comp;
    FNonWindowsPlayCommand: string;
    FHTTPUserName: string;
    FHTTPPassword: string;
    FProxyUserName: string;
    FProxyPassword: string;
    FProxyHost: string;
    FProxyPort: integer;
    FRootPath: string;
    FIconsPath: string;
    FSoundsPath: string;
    FPopupLevels: string;
    FPopupString: string;
    FColor: string;
    FTitle: string;
    FCycleIndex: integer;
    FPollTime: integer;
    FPageDelay: integer;
    FHttpClient: TFPHTTPClient;
    IniPath: string;
    function GetTimeStamp: comp;
    procedure ShowStatus(NewStatus: string);
    procedure PlayStatus(NewStatus: string);
    procedure LoadConfigSection(Ini: TIniFile; SectionName: string);
    procedure LoadConfig(SectionName: string);
    procedure LoadIcons();
    function ProcessFields(FmtStr: string): string;
    { private declarations }
  public
    { public declarations }
    FDisplayURL: string;
  end;

var
  MainForm: TMainForm;

implementation

uses MessageForm;

const
  MAXCOLORS = 4;
  ColorChars = 'yprg';
  ColorColors: array[1..MAXCOLORS] of TColor =
    (clYellow, clPurple, clRed, clGreen);
  ColorNames: array[1..MAXCOLORS] of string =
    ('yellow', 'purple', 'red', 'green');

var
  FIconList: array[1..MAXCOLORS] of TIcon;

{$R *.lfm}

procedure TMainForm.ShowStatus(NewStatus: string);
var
  mycolor: TColor;
begin
  myMessageForm.Close;
  mycolor := ColorColors[Pos(NewStatus, ColorChars)];
  if Pos(NewStatus, FPopupLevels) > 0 then
  begin
    myMessageForm.Icon := TrayIcon.Icon;
    if FPopupString = '' then
      myMessageForm.ShowMesg(FTitle, mycolor)
    else
      myMessageForm.ShowMesg(ProcessFields(FPopupString), mycolor);
  end;
  if (NewStatus = 'r') then
    FirstAlertTime := GetTimeStamp();
end;

function RemoveComment(S: string): string;
var
  I: integer;
begin
  I := Pos(';', S);
  if I > 1 then
    SetLength(S, I - 1);
  Result := Trim(S);
end;

{ TMainForm }

procedure TMainForm.LoadIcons;
var
  I: integer;
  FN: string;
  AnIcon: TIcon;
begin
  for I := 1 to MAXCOLORS do
  begin
    FN := FIconsPath + ColorChars[I] + '.ico';
    if FileExists(FN) then
    begin
      try
        AnIcon := TIcon.Create;
        AnIcon.LoadFromFile(FN);
        if (FIconList[I] <> nil) then
          FIconList[I].Free;
        FIconList[I] := AnIcon;
      except
        on E: Exception do
        begin
          ShowMessage(E.Message);
        end;
      end;
    end
    else begin
      ShowMessage('File does not exist: ' + FN);
      Halt;
    end;
  end;
end;

procedure TMainForm.TrayIconDoubleClick(Sender: TObject);
begin
{$IFDEF Windows}
  // 5=SW_SHOW
  ShellExecute(Application.MainForm.Handle, nil, PChar(FDisplayURL), nil, nil, 5);
{$ENDIF}
end;

procedure TMainForm.LoadConfigSection(Ini: TIniFile; SectionName: string);
begin
  FIconsPath := IncludeTrailingPathDelimiter(RemoveComment(Ini.ReadString(SectionName, INIICONSPATH, FIconsPath)));
  FSoundsPath := IncludeTrailingPathDelimiter(
    RemoveComment(Ini.ReadString(SectionName, INISOUNDSPATH, FSoundsPath)));
  FDisplayURL := RemoveComment(Ini.ReadString(SectionName, INIDISPLAYURL, FDisplayURL));
  FHTTPUserName := RemoveComment(Ini.ReadString(SectionName, INIHTTPUSERANME, FHTTPUserName));
  FHTTPPassword := RemoveComment(Ini.ReadString(SectionName, INIHTTPPASSWORD, FHTTPPassword));
  FProxyUserName := RemoveComment(Ini.ReadString(SectionName, INIPROXYUSERANME, FProxyUserName));
  FProxyPassword := RemoveComment(Ini.ReadString(SectionName, INIPROXYPASSWORD, FProxyPassword));
  FProxyHost := RemoveComment(Ini.ReadString(SectionName, INIPROXYHOST, FProxyHost));
  FProxyPort := Ini.ReadInteger(SectionName, INIPROXYPORT, FProxyPort);
  FPopupLevels := RemoveComment(Ini.ReadString(SectionName, INIPOPUPLEVELS, FPopupLevels));
  FPollTime := Ini.ReadInteger(SectionName, INIPOLLTIME, FPollTime);
  FPageDelay := Ini.ReadInteger(SectionName, INIPAGEDELAY, FPageDelay);
  FNonWindowsPlayCommand := RemoveComment(Ini.ReadString(SectionName, INIPLAYCOMMAND, FNonWindowsPlayCommand));
end;

procedure TMainForm.LoadConfig(SectionName: string);
var
  Ini: TIniFile;
  proxy: TProxyData;
begin
  FRootPath := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)));
  FIconsPath := IncludeTrailingPathDelimiter(FRootPath + DEF_RESOURCEPATH);
  FSoundsPath := IncludeTrailingPathDelimiter(FRootPath + DEF_RESOURCEPATH);
  FPopupLevels := POPUPLEVELS;
  FPollTime := DEF_POLLTIME;
  FPageDelay := DEF_PAGEDELAY;
  FDisplayURL := '';
  FHTTPUserName := '';
  FHTTPPassword := '';
  FProxyHost := '';
  FProxyPort := DEF_PROXYPORT;
  FProxyUserName := '';
  FProxyPassword := '';
  FNonWindowsPlayCommand := '';

  if (FileExists(IniPath)) then
  begin
    Ini := TIniFile.Create(IniPath, [ifoStripQuotes]);
    LoadConfigSection(Ini, DEF_SECTION);
    if (SectionName <> '') then
      LoadConfigSection(Ini, SectionName);
  end;

  if (FHTTPUserName <> '') then
  begin
    FHttpClient.UserName := FHTTPUserName;
    FHttpClient.Password := FHTTPPassword;
  end;

  if (FProxyHost <> '') then
  begin
    proxy := TProxyData.Create;
    proxy.Host := FProxyHost;
    proxy.Port := FProxyPort;
    if (FProxyUserName <> '') then
    begin
      proxy.UserName := FProxyUserName;
      proxy.Password := FProxyPassword;
    end;
    FHttpClient.Proxy := proxy;
  end;

  LoadIcons();
  MainTimer.Interval := FPollTime * 1000;

end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  headers: TStringList;
begin
  FHttpClient := TFPHTTPClient.Create(nil);
  headers := TStringList.Create;
  headers.Add(HeaderUserAgent + ': OMTray/' + VERSION);
  FHttpClient.RequestHeaders := headers;

  MainTimer.Enabled := False;
  CycleTimer.Enabled := False;
  IniPath := ExtractFilePath(ParamStr(0)) + INIFILE;
  LoadConfig(ParamStr(1));

  {
  TrayIcon.BalloonHint:='OMTray Starting';
  TrayIcon.Visible:=true;
  TrayIcon.ShowBalloonHint;
  }

  FCycleIndex := 0;
  CycleTimer.Interval := 250;
  CycleTimer.Enabled := True;
  CycleTimerTimer(CycleTimer);

  MainTimerTimer(MainTimer);
  MainTimer.Enabled := True;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FHttpClient.Free;
end;

procedure TMainForm.CycleTimerTimer(Sender: TObject);
begin
  CycleTimer.Enabled := False;
  TrayIcon.Icon.Assign(FIconList[FCycleIndex + 1]);
  FCycleIndex := FCycleIndex + 1;
  if (FCycleIndex = MAXCOLORS) then
    FCycleIndex := 0;
  CycleTimer.Enabled := True;
end;

procedure TMainForm.EditIniFileOnClick(Sender: TObject);
begin
{$IFDEF Windows}
  // 5=SW_SHOW
  ShellExecute(Application.MainForm.Handle, nil, PChar(INIFILE), nil, nil, 5);
{$ENDIF}
end;

procedure TMainForm.AboutOnClick(Sender: TObject);
begin
  MessageDlg('OMTray ' + VERSION + LineEnding + LineEnding + 'All of Big Brothers companion' +
    LineEnding + 'By Oles Hnatkevych and ' + 'based on BBTRay by Deluan (bbtray@deluan.com.br)',
    mtInformation, [mbOK], 0);
end;

procedure TMainForm.CheckInstantlyOnClick(Sender: TObject);
begin
  MainTimerTimer(MainTimer);
end;

procedure TMainForm.MainTimerTimer(Sender: TObject);
var
  pagehtml: string;
  I, J: integer;
  timePassed: comp;
  LastStatus, NewStatus: string;
  ParseError: boolean;
begin
  MainTimer.Enabled := False;
  TrayIcon.Hint := 'Verifying';
  LastStatus := Copy(FTitle, 1, 1);

  if (FDisplayURL = '') then
    TrayIcon.Hint := INIDISPLAYURL + ' not configured'
  else if (Pos('http', FDisplayURL) <> 1) then
    TrayIcon.Hint := 'Wrong ' + INIDISPLAYURL + ': ' + FDisplayURL
  else
  begin
    try
      pagehtml := FHttpClient.Get(FDisplayURL);
    except
      on E: EHTTPClient do
      begin
        TrayIcon.Hint := E.Message;
        CycleTimer.Enabled := True;
      end;
      on E: ESocketError do
      begin
        TrayIcon.Hint := E.Message;
        CycleTimer.Enabled := True;
      end;
      on E: Exception do
      begin
        TrayIcon.Hint := E.Message;
        CycleTimer.Enabled := True;
      end;
    end;
  end;

  ParseError := False;
  if (pagehtml <> '') then
  begin
    I := Pos('<TITLE>', pagehtml);
    J := Pos('</TITLE>', pagehtml);
    if (I * J) = 0 then
    begin
      TrayIcon.Hint := 'Invalid page';
      CycleTimer.Enabled := True;
      ParseError := True;
    end
    else
    begin
      Inc(I, 7);
      pagehtml := Copy(pagehtml, I, J - I);
      FTitle := pagehtml;
      I := Pos(pagehtml[1], ColorChars);
      if (I = 0) then
      begin
        CycleTimer.Enabled := True;
        TrayIcon.Hint := 'Invalid title';
        ParseError := True;
      end
      else
      begin
        CycleTimer.Enabled := False;
        FColor := ColorNames[I];
        TrayIcon.Icon.Assign(FIconList[I]);
        TrayIcon.Hint := FTitle;
      end;
    end;
  end;
  TrayIcon.Show;
  NewStatus := Copy(FColor, 1, 1);
  timePassed := GetTimeStamp() - FirstAlertTime - FPageDelay * 1000;
  if ((LastStatus = '') or ParseError) then
  // do nothing
  else if (LastStatus <> NewStatus) then
  begin
    ShowStatus(NewStatus);
    PlayStatus(NewStatus);
  end
  else if ((LastStatus = 'r') and (timePassed > 0)) then
  begin
    ShowStatus(NewStatus);
    PlayStatus(NewStatus);
  end;

  MainTimer.Enabled := True;
end;

procedure TMainForm.ShowPopupClick(Sender: TObject);
var
  LastStatus: string;
begin
  LastStatus := Copy(FTitle, 1, 1);
  ShowStatus(LastStatus);
  PlayStatus(LastStatus);
end;

procedure TMainForm.OnExitClick(Sender: TObject);
begin
  Halt(0);
end;

procedure TMainForm.OnReloadIniOnClick(Sender: TObject);
begin
  LoadConfig(ParamStr(1));
  MainTimerTimer(MainTimer);
end;

procedure TMainForm.OpenInBrowserClick(Sender: TObject);
begin
  TrayIconDoubleClick(Sender);
end;

function TMainForm.ProcessFields(FmtStr: string): string;
var
  I: integer;
  Ident: char;
  Value: string;
begin
  Result := FmtStr;
  I := Pos('%', Result);
  while I > 0 do
  begin
    Delete(Result, I, 1);
    if Result[I] <> '%' then
    begin
      Ident := Result[I];
      Delete(Result, I, 1);
      case Ident of
        'U': Value := FDisplayURL;
        'c': Value := Copy(FColor, 1, 1);
        'C': Value := FColor;
        'T': Value := FTitle;
        else
          Value := '';
      end;
      Insert(Value, Result, I);
    end;
    I := Pos('%', Result);
  end;
  I := Pos('\n', Result);
  while I > 0 do
  begin
    Delete(Result, I, 2);
    Insert(#10, Result, I);
    I := Pos('\n', Result);
  end;
end;

function TMainForm.GetTimeStamp(): comp;
begin
  Result := TimeStampToMSecs(DateTimeToTimeStamp(Now));
end;

procedure TMainForm.PlayStatus(NewStatus: string);
var
  WavPath: string;
{$IFNDEF Windows}
  AProcess: TProcess;
{$ENDIF}
begin
  WavPath := FSoundsPath + NewStatus + '.wav';
  if (FileExists(WavPath)) then
  begin
{$IFDEF Windows}
    PlaySound(PChar(WavPath), 0, SND_FILENAME or SND_ASYNC);
{$ELSE}
    AProcess := TProcess.Create(nil);
    AProcess.Executable := FNonWindowsPlayCommand;
{$ENDIF}
  end;
end;

end.
