unit MessageForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, main;

type

  { TMessageForm }

  TMessageForm = class(TForm)
    OpenButton: TButton;
    GotItButton: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure GotItOnClick(Sender: TObject);
    procedure OnKeyPress(Sender: TObject; var Key: char);
    procedure Label1Click(Sender: TObject);
    procedure OnButtonKeyPress(Sender: TObject; var Key: char);
    procedure OpenOnClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    procedure ShowMesg(msg: string; mycolor: TColor);
  end;

var
  myMessageForm: TMessageForm;

implementation

{$R *.lfm}

procedure TMessageForm.FormCreate(Sender: TObject);
begin

end;

procedure TMessageForm.GotItOnClick(Sender: TObject);
begin
  Close;
end;

procedure TMessageForm.OnKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #27 then
    Close
  else if Key = #13 then
    Label1Click(Sender);
end;

procedure TMessageForm.Label1Click(Sender: TObject);
begin
  MainForm.TrayIconDoubleClick(Sender);
  Close;
end;

procedure TMessageForm.OnButtonKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #27 then
    Close
end;

procedure TMessageForm.OpenOnClick(Sender: TObject);
begin
  MainForm.TrayIconDoubleClick(Sender);
  Close
end;


procedure TMessageForm.ShowMesg(msg: string; mycolor: TColor);
begin
  Label1.Caption := msg;
  Label1.Color := mycolor;
  Color := mycolor;
  Show;
end;

end.

