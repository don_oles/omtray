# OMTray What is it

This is an adaptation of [BBTray](https://gitlab.com/don_oles/BBtray) to modern reality, i.e. Lazarus and modern SSL/TLS. It's a tray program that monitors OMonitor page (or Big Brother, or Xymon) and alerts user with popup notifications and sound when status changes.

Monitoring program that notifies via emails and SMS is not enough, messages are easy to miss, but an active monitoring program that gets attention in a matter of secons is what system and network administrators need to know of a failure before customers complain.

The main problem of the old BBTray is the obsolete SSL libraries which do not understand SNI extention to TLS, and another problem is that it compiles with the ancient Delphi.


# How to install

You need in a separate folder the executable (omtray.exe), the 'resource' subfolder with icons and wav files, and omtray.ini.

The omtray.ini requires only DisplayURL, other paramters can be optional. See omtray.ini.DIST for more details what you can have there.
```
[Default]
DisplayURL=https://omonitor.company.com/
```

# TODO

Make it run on Linux. Sound can be played by calling extenral command line players. The challenge is that TTrayIcon does not work properly.


